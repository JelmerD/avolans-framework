<?php
/**
 * Author: Jelmer Dröge
 * Date: 11-6-12
 * Time: 23:16
 * Copyright: 2012(c) Avolans.nl
 */
class AvolansFramework
{

    /**
     * Construct the MVC framework.
     */
    public function __construct()
    {
        /**
         * The root path of the application
         */
        define('__PATH__', dirname(__FILE__) . DIRECTORY_SEPARATOR);
        define('START', microtime(true));
        require_once(__PATH__ . 'app/Registry.php');
        require_once(__PATH__ . 'app/Router.php');

        //Make the script execute a function when the script ends
        register_shutdown_function(array($this, 'scriptEnd'));

        new ConfigModel();
        new Router();
    }

    /**
     * Gets executed on script end, die() exit() and so forth.
     */
    public function scriptEnd(){
        LogModel::appendLogToFile();
    }

}

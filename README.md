Features
========

Todo
====
 - Design a nice way to get if it's a 404 or not
 - Add Log functionality
 - Get all the Models working
 - Add js and css functions to TemplateView
 - Make a nice custom template to test everything
 - Rewrite/update .htaccess file

License
=======
Copyright (c) 2012, Jelmer Dröge
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
(http://www.opensource.org/licenses/bsd-license.php)

How to use
==========
## Template
Give each template it's own folder and add your html, css and js files like you allways do. You can use the template class by adding custom variables to the template.

Variables are used like below.

 - {% var %} => replace this var by content out a variable
 - {% include file.html %} => include another file from the template root
 - {% include ../file.html false %} => include another file from this relative file path

You can even use template-variable-ception :) . For example the template variable 'title' = 'Homepage', then you can use {% title %} to echo that variable.
But you can also say that template variable 'welcome' = 'Welcome to the {% title %}' which will output: 'Welcome to the Homepage'.
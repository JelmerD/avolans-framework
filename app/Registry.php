<?php
/**
 * Author: Jelmer Dröge
 * Date: 12-4-12
 * Time: 22:12
 * Copyright: 2012(c) Avolans.nl
 */
final Class Registry extends stdClass
{

    # IDE hack, the variables below are created so the IDE will recognize them and show them while typing :)
    public $pageVar;
    public $conf;
    public $params;
    public $db;

    /**
     * @var Registry $instance
     * @static
     * Singleton instance
     */
    private static $instance = null;

    /**
     * @access private
     * @final
     *         Make sure we cant call this function outside itself (Singleton)
     */
    private final function __construct(){

    }

    /**
     * getInstance
     *
     * @static
     * @return Registry
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new Registry();
        }
        return self::$instance;
    }

}

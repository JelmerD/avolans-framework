<?php
/**
 * Author: Jelmer Dröge
 * Date: 11-6-12
 * Time: 23:01
 * Copyright: 2012(c) Avolans.nl
 */
class Router
{

    /**
     * Contains the complete paths.ini
     * @var array $ini_all
     */
    private $ini_all;

    /**
     * Contains the part of the paths.ini that is written for this page
     * @var array $ini
     */
    private $ini;

    /**
     * Contains the current url. So everything after the main domain ($_GET['url'])
     * @var string $url
     */
    private $url;

    /**
     * Constructor function for the Router
     */
    public function __construct()
    {
        $this->reg = Registry::getInstance();

        $this->ini_all = parse_ini_file(__PATH__ . 'config/paths.ini', true);
        $this->ini_all = array_change_key_case($this->ini_all, CASE_LOWER);

        $this->url = strtolower(rtrim($_GET['url'], '/'));

        $this->matchURL();
        $this->setParameters();
        $this->initController($this->ini['controller']);
    }

    /**
     * Check if the current url matches one of the patterns defined in the paths.ini file to see which controller it has
     * to use. It will set the $this->ini to the array of the match.
     * @return bool
     */
    private function matchURL(){
        foreach ($this->ini_all as $value){
            if (!isset($value['url'])){
                continue;
            }
            //#TODO put the rules in a new .ini file so it will be easier to change the rules
            $regex = preg_replace('~\*~', '[a-zA-Z0-9_/]+', $value['url']);
            $regex = preg_replace('~\.~', '\.', $regex);
            preg_match('~^' . $regex . '$~', $this->url, $match);
            if (!empty($match)){
                $this->ini = $value;
                return true;
            }
        }
        $this->ini = $this->ini_all['default'];
        return false;
    }

    /**
     * Push the parameters in the Registry. It depends on the path that might have been set in the paths.ini file to
     * set either the simulated path as parameters or the real path.
     */
    private function setParameters(){
        if (isset($this->ini['path'])){
            $this->reg->params = explode('/', $this->ini['path']);
        } else {
            $this->reg->params = explode('/', $this->url);
        }
    }

    /**
     * This function initializes the correct controller. If it does not exist it will kill itself and you will be notified.
     * @param $controller String Name of the controller to load
     */
    private function initController($controller){
        $controller = ucfirst($controller) . 'Controller';
        if (!file_exists(__PATH__ . 'app/controller/' . $controller . '.php')){
            die('The ' . $controller . ' does not exist. Please change the config/paths.ini file.');
        }
        new $controller();
    }



}

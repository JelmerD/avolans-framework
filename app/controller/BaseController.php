<?php
/**
 * Author: Jelmer Dröge
 * Date: 12-4-12
 * Time: 21:23
 * Copyright: 2012(c) Avolans.nl
 */

class BaseController
{

    /**
     * @var Registry registry for further use in the controller
     */
    protected $reg;

    public function __construct(){
        $this->reg = Registry::getInstance();
        $this->init();
    }

    protected function init(){

    }

}

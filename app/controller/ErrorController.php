<?php
/**
 * Author: Jelmer Dröge
 * Date: 12-4-12
 * Time: 21:45
 * Copyright: 2012(c) Avolans.nl
 */

class ErrorController extends BaseController
{

    protected function init(){
        $this->reg->pageVar->title = "error :: " . $this->reg->params[1];
        $view = new TemplateView('error');
        $view->display();
    }

}

<?php
/**
 * Author: Jelmer Dröge
 * Date: 12-4-12
 * Time: 21:45
 * Copyright: 2012(c) Avolans.nl
 */

class PageController extends BaseController
{

    protected function init(){
        if ($this->reg->params[1] == 'index'){
            //some test variables
            $this->reg->pageVar->title = 'Avolans Framework';
            $this->reg->pageVar->page = 'Home';
            $this->reg->pageVar->content = '<h1>Welcome {% username %} to {% title %}</h1>';
//            $this->reg->pageVar->nav = array('Homepage' => 'home.html', 'About' => 'about.html');
            $this->reg->pageVar->username = 'Jelmer Dr&ouml;ge';

            //This is where the database connection get's initialised
            $this->reg->db = new DatabaseModel();

            $view = new TemplateView();
            $view->display();
        } else {
            var_dump($this->reg->params);
        }

    }

}

<?php
/**
 * Author: Jelmer Dröge
 * Date: 16-4-12
 * Time: 23:16
 * Copyright: 2012(c) Avolans.nl
 */
class BaseModel
{

    /**
     * @var Registry registry for further use in the controller
     */
    protected $reg;

    public function __construct()
    {
        $this->reg = Registry::getInstance();
        $this->init();
    }

    protected function init(){

    }

}

<?php
/**
 * Author: Jelmer Dröge
 * Date: 18-4-12
 * Time: 22:37
 * Copyright: 2012(c) Avolans.nl
 */
class ConfigModel extends BaseModel
{

    /**
     * @var array $ini The config.ini file
     */
    private $ini;

    protected function init(){
        $this->readIni();
    }

    /**
     * Read the config.ini file and push it in the $ini
     * @return bool true on success
     */
    private function readIni(){
        if (!file_exists(__PATH__ . 'config/config.ini')){
            LogModel::w('FATAL', 'The config.ini file does not exist');
            return false;
        } else {
            if (!$this->ini = parse_ini_file(__PATH__ . 'config/config.ini', true)){
                LogModel::w('FATAL', 'The config.ini file could not be read');
                return false;
            }
        }
        $this->addToRegistry();
        return true;
    }

    /**
     * Push all the config values to the registry for further use throughout the script.
     */
    private function addToRegistry(){
        foreach ($this->ini as $k => $v){
            $this->reg->conf->$k = $v;
        }
    }

}

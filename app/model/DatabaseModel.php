<?php
/**
 * Author: Jelmer Dröge
 * Date: 12-4-12
 * Time: 23:05
 * Copyright: 2012(c) Avolans.nl
 */
class DatabaseModel extends BaseModel
{

    /**
     * @var mysqli $conn The connection
     */
    private $conn = NULL;

    /**
     * @var string $q The query to run
     */
    private $q = NULL;

    /**
     * @var mysqli_result $result The result of the last query
     */
    private $result = NULL;

    /**
     * @var array $return The array with all the results
     */
    private $return = array();

    protected function init(){
        $this->connect();
    }

    /**
     * Create a connection with the database
     */
    private function connect(){
        $this->conn = @new mysqli($this->reg->conf->database['host'], $this->reg->conf->database['user'], $this->reg->conf->database['password'], $this->reg->conf->database['database']);
        if ($this->conn->connect_errno){
            LogModel::w('FATAL', 'Can not connect to database', 'Error:' . $this->conn->connect_error);
        }
        $this->select('*', 'test');
        var_dump($this->q);
        var_dump($this->return);
    }

    /**
     * Run a query and push the results to the return variable
     * @param null $query string Define a custom query, this will overwrite any predefined queries in $this->q
     * @return array The complete result including some additional information
     */
    public function query($query = null){
        $this->q = (isset($query)) ? $query : $this->q;
        LogModel::w('QUERY', $this->q);
        $this->result = @$this->conn->query($this->q);
        if ($this->conn->errno){
            //Something went wrong
            LogModel::w('FATAL', 'Something went wrong while trying to access the database', 'Error:' . $this->conn->error);
            $this->return['success'] = false;
            $this->return['errno'] = $this->conn->errno;
        } else {
            //Everything went good
            $this->return['success'] = true;
            $this->return['affected'] = $this->conn->affected_rows;
            $this->return = array_merge($this->return, $this->result->fetch_all());
            $this->result->close();
        }
        LogModel::w('QUERY_RESULT', $this->return);
        return $this->return;
    }

    /**
     * @param $what
     * @param $from
     * @param null $advanced
     * @return array
     */
    public function select($what, $from, $advanced = null){
        $what = $this->csv($what);
        $this->q = 'SELECT ' . $what . ' FROM ' . $this->prefix($from) . ' ' . $advanced;
        $this->query();
        return $this->return;
    }

    public function insert(){

    }

    public function update(){

    }

    public function deleteRow(){

    }

    /**
     * @param $table string The table to prefix
     * @return string The prefixed table name
     */
    private function prefix($table){
        return $this->reg->conf->database['prefix'] . $table;
    }

    /**
     * Escape a value with the real_escape_string function
     * @param $n string|array The input string that has to be escaped
     * @return String|array the escaped value
     */
    public function safe($n){
        if (is_array($n)){
            foreach($n as $k => $v){
                $n[$k] = $this->safe($v);
            }
        }
        return $this->conn->real_escape_string($n);
    }

    /**
     * Make a string of an array separated by commas
     * @param $arr string|array The array that needs to be separated by commas
     * @return string The array separated by commas
     */
    public function csv($arr){
        if (is_array($arr)){
            return '`' . implode('`, `', $arr) . '`';
        }
        return $arr;
    }

}

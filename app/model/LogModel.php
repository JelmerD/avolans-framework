<?php
/**
 * Author: Jelmer Dröge
 * Date: 12-4-12
 * Time: 23:04
 * Copyright: 2012(c) Avolans.nl
 */
class LogModel extends BaseModel
{

    /**
     * @var LogModel $instance
     * @static
     * Singleton instance
     */
    private static $instance = null;

    /**
     * @var array $data The data that is being written
     */
    private $data = array();

    /**
     * We use this information to determine who the caller of the function is, so we can log this
     * @var Array $bt Backtracking information
     */
    private $bt;

    /**
     * @var String $logFile The full path to the logfile
     */
    private $logFile;

    /**
     * @var int $indent The amount of spaces to add per indent level
     */
    private $indent = 2;

    /**
     * All the logmessages so we can write everything all at once.
     * @var string $logMessages
     */
    private $logMessages = null;

    /**
     * Alias for write()
     * @param string $level
     * @param Mixed $message [optional] The messages, separated by commas
     * @return bool Boolean,  true if write access, false if not
     * @see write()
     */
    public static function w($level = NULL, $message = NULL){
        $log = self::getInstance();
        //If the alias is used, define the bt information already so we can use key 0 as the caller
        $log->bt = debug_backtrace();
        return $log->write(func_get_args());
    }

    /**
     * @param string $level
     * @param Mixed $message [optional] The messages, separated by commas
     * @return bool Boolean,  true if write access, false if not
     * @example
     * <code>
     * <?php
     * Log::write('ERROR', 'something went wrong in the array:', $array)
     * ?>
     * </code>
     */
    public static function write($level = NULL, $message = NULL){
        $log = self::getInstance();
        //If the alias isn't used, but the real function, define the caller
        $toLog = func_get_arg(0);
        if (!isset($log->bt)){
            $log->bt = debug_backtrace();
            $toLog = func_get_args();
        }
        $log->data['file'] = preg_replace('~^' . addslashes(__PATH__) . '~', '', $log->bt[0]['file']);
        $log->data['line'] = $log->bt[0]['line'];
        $log->data['level'] = strtoupper($toLog[0]);
        if (!$log->checkLevel()){
            return false;
        }
        $logMessage = '';
        if ($log->reg->conf->log['detailed']){
            foreach ($toLog as $k => $v){
                if ($k == 0){
                    continue;
                }
                $logMessage .= $log->getLogMessage($v);
            }
        }
        $log->appendData($logMessage);
        return true;
    }

    /**
     * Check if the loglevel specified in the write function is in the config file to see if the message
     * is approved to be logged
     * @return bool
     * @see write()
     */
    private function checkLevel(){
        if ($this->reg->conf->log['allow_all']){
            return true;
        }
        if (in_array($this->data['level'], $this->reg->conf->log['levels'])){
            return true;
        }
        return false;
    }

    /**
     * Get the log message for each and every single argument > 0 specified in the write function
     * @param $v Mixed An array or string that has to be added to the log
     * @return string The data as string
     * @see write()
     * @see logArray()
     */
    private function getLogMessage($v){
        if (is_array($v)){
            $data = $this->logArray($v);
            $dTemp = explode(PHP_EOL, $data);
            $data = '';
            $row = 1;
            for ($i = 0; $i < count($dTemp) - 1; $i++){
                $data .= $this->right(1, 3) . $row . '.' . str_repeat(' ', 5 - strlen($row)) . $dTemp[$i] . PHP_EOL;
                $row++;
            }
        } else {
            $data = $this->right(1) . $v . PHP_EOL;
        }

        return $data;
    }

    /**
     * Append data to the .log file
     * @param $data String The data thas has to be appended to the file
     */
    private function appendData($data){
        $this->logFile = __PATH__ . '/logs/' . date('Ymd') . '.log';
        $this->createLogFile();
        $header = date('H:i:s P') . ' ' . round((microtime(true) - START)*1E6)/1E3 . 'ms' . ' [' . $this->data['level'] . '] @ ' . $this->data['file'] .
            ' on line ' . $this->data['line'] . PHP_EOL;

        //Log the messages all at once, or message by message
        if ($this->reg->conf->log['append_at_once']){
            $this->logMessages .= '= ' . $header . $data . PHP_EOL;
        } else {
            file_put_contents($this->logFile, '= ' . $header . $data . PHP_EOL, FILE_APPEND);
        }
    }

    /**
     * Create a .log file if it does not yet exist
     * @return bool True on success, False on failure
     */
    private function createLogFile(){
        if (file_exists($this->logFile)){
            return true;
        }
        touch($this->logFile);

        $initData = 'CREATED AT:    ' . date('r') . PHP_EOL;
        if ($this->reg->conf->log['allow_all']){
            $initData .= 'ALLOWED LOGS:  EVERYTHING IS ALLOWED (allow_all = 1)' . PHP_EOL;
        } else {
            $initData .= 'ALLOWED LOGS:  ' . implode(', ',$this->reg->conf->log['levels']) . PHP_EOL;
        }
        file_put_contents($this->logFile, $initData . PHP_EOL);
        return true;
    }

    /**
     * getInstance
     * @static
     * @return LogModel
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new LogModel();
        }
        return self::$instance;
    }

    /**
     * Retrieve an X amount of spaces, for some extra offset in the logfile
     * @param $count int the Amount of steps you'd like to indent
     * @param $add int Add a number of spaces you want extra
     * @return string
     */
    private function right($count = 0, $add = 0){
        return str_repeat(' ', $count * $this->indent + $add);
    }

    /**
     * Arrays are hard to log. That's why this function exists. This functions makes a readable array with indent
     * that can be appended to the file. It calls itself when it notices that a value of the array is another array.
     * @param $arr Array The array that needs to become a string
     * @param int $depth The depth of the function. This is only used by the function itself in order to retrieve
     * the correct amount of indent.
     * @return string The array as string.
     */
    private function logArray($arr, $depth = 0){
        $s = $this->right($depth);
        $sPlu = $this->right($depth, 3);
        $str = 'Array(' . PHP_EOL;
        foreach ($arr as $k => $v){
            $str .= $sPlu . '[' . $k . '] => ';
            //if the value is another array, run this function but with $depth + 1
            if (is_array($v)){
                $str .= $this->logArray($v, $depth + 1);
                continue;
            }
            $str .= $v . PHP_EOL;
        }
        $str .= $s . ')' . PHP_EOL;
        return $str;
    }

    /**
     * Append the logMessages to the logfile. This should reduce the amount of resources needed by the file_put_contents
     * because now we add all the messages all at once instead of per logmessage.
     * @static
     */
    public static function appendLogToFile(){
        $log = self::getInstance();
        if ($log->logMessages && $log->reg->conf->log['append_at_once']){
            file_put_contents($log->logFile, str_repeat('-', 90) . PHP_EOL . $log->logMessages, FILE_APPEND);
        }
    }

}

<?php
/**
 * Author: Jelmer Dröge
 * Date: 13-4-12
 * Time: 9:10
 * Copyright: 2012(c) Avolans.nl
 */
class BaseView
{

    /**
     * @var Registry registry for further use in the controller
     */
    protected $reg;

    /**
     * @var String page content which will be echo'd when display() is called
     */
    protected $pageContent;

    /**
     * Construct a controller
     */
    public function __construct(){
        $this->reg = Registry::getInstance();
        $this->init();
    }

    protected function init(){

    }

    /**
     * Replace some content in the pageContent variable. For example a variable, or some content from an included file.
     * @param $from String what would you like to change?
     * @param $to String to what would like it to change?
     */
    protected function replaceContent($from, $to){
        $this->pageContent = str_replace($from, $to, $this->pageContent);
    }

    /**
     * Call this function when you want to echo the output. This is the only echo in the whole project
     */
    public function display(){
        echo $this->pageContent;
    }

}

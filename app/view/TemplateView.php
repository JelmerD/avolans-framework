<?php
/**
 * #TODO What do we do when we want a slightly different look for another page? Take the base of a main template?
 * #TODO Or are we going to copy paste all of the basic structure? Hmmm
 * #TODO Add css and javascript functionality (overlaying all the html files), but make it optional to disable it at pages for performance
 * Author: Jelmer Dröge
 * Date: 13-4-12
 * Time: 11:19
 * Copyright: 2012(c) Avolans.nl
 */
class TemplateView extends BaseView
{
    /**
     * @var array All the available variables inside the template
     */
    private $vars = Array();

    /**
     * @var string $begVar The opening part for a template variable
     */
    private $begVar = '{%';

    /**
     * @var string $endVar The closing part for a template variable
     */
    private $endVar = '%}';

    /**
     * @var string template root
     * @example example.com/templates/mytemplate/
     */
    private $templateLoc;


    /**
     * Construct a TemplateView with default template example. If no template is found it will simply throw an error.
     * It will always start building from the index.html, so just build a website like you allways do.
     * @return \TemplateView false if it can't load the template
     */
    protected function init()
    {
        $template = 'example';
        //If the begin and/or endvar of the template are defined in the config file, use them
        $this->begVar = (isset($this->reg->conf->template['begVar'])) ? $this->reg->conf->template['begVar'] : $this->begVar;
        $this->endVar = (isset($this->reg->conf->template['endVar'])) ? $this->reg->conf->template['endVar'] : $this->endVar;
        $this->vars = $this->reg->pageVar;
        $this->templateLoc = __PATH__ . 'templates/' . $template . '/';
        if (!file_exists($this->templateLoc . 'index.html')){
            LogModel::w('FATAL', 'The template "' . $this->templateLoc . '" does not exist.\n
                Please check if the template folder "' . $template . '" exists and there is an index.html file included');
            return false;
        }

        $this->pageContent = file_get_contents($this->templateLoc . 'index.html');
        $this->getVariables($this->pageContent);
        return true;
    }

    /**
     * Check all the custom template variables in the template file you specified.
     * @param $file String file content you'd like to check for template variables
     * @return bool true if it has variables, false if it has not
     */
    private function getVariables($file){
        $regex = '~' . $this->begVar . '([^' . $this->endVar . ']+)' . $this->endVar . '~';
        if (preg_match_all($regex, $file, $matches)){
            foreach ($matches[1] as $k => $v){
                $v = trim($v);
                $expl = explode(' ', $v);
                //Check what to do with the exploded var
                switch (count($expl)){
                    case 0:
                        //do nothing, the variable is empty
                        break;
                    case 1:
                        //this means it is just a variable, since there is no keyword
                        $this->replaceVariable($expl[0], $matches[0][$k]);
                        break;
                    default:
                        //there must be a keyword, ie. explode, since there are >1 parameters
                        $this->getParameters($expl, $matches[0][$k]);
                        break;
                }
            }
            return true;
        }
        return false;
    }

    /**
     * Replace a template variable of type VARIABLE to the in the controller specified content
     * @param $var String Variable you want to replace by content
     * @param $original String Original variable
     * @return bool false if the variable doesn't exist
     */
    private function replaceVariable($var, $original){
        if ( !isset($this->vars->$var) ){
            LogModel::w('WARNING', 'Variable "' . $var . '" does not exist.');
            return false;
        }
        $this->replaceContent($original, $this->vars->$var);

        /** added so you can use template variables inside template variables
         * example: title = 'Homepage' and welcome = 'Welcome to {% title %}'
         * output of {% title %} => Welcome to Homepage
         */
        $this->getVariables($this->vars->$var);

        return true;
    }

    /**
     * If a template variable has 2 or more strings, it means it has parameters. This function will get those and
     * parse the parameters to the correct sub function.
     * @param $param Array All the parameters
     * @param $original String Original variable
     */
    private function getParameters($param, $original){
        switch (strtolower($param[0])){
            case 'var':
                $this->replaceVariable($param[1], $original);
                break;
            case 'include':
                $this->includeFile($param, $original);
                break;
        }
    }

    /**
     * Include a file from the root of the template
     * Set the 3rd parameter in the template to 'false' if you want the path to the other file to be relative
     * @param $param Array all the parameters
     * @param $original String Original variable
     * @return bool true at success, false at fail
     */
    private function includeFile($param, $original){
        $filepath = $param [1];
        $fromroot = true;
        if (isset($param[2]) AND ($param[2] == 'false')){
            $fromroot = false;
        }

        if( $fromroot ){
            # TODO variable check class
            $filepath = $this->templateLoc . $filepath;
        }

        if (!file_exists($filepath)){
            LogModel::w('WARNING', 'Include file "' . $filepath . '" does not exist.');
            return false;
        }
        $content = file_get_contents($filepath);
        $this->replaceContent($original, $content);
        $this->getVariables($content);
        return true;
    }

}

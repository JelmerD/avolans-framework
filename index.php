<?php
/**
 * Author: Jelmer Dröge
 * Date: 12-4-12
 * Time: 20:19
 * Copyright: 2012(c) Avolans.nl
 */

/**
 * Autoload classes from model and view
 * @param $file String class name
 */
function __autoload($file){
    //Load all the Models
    if (preg_match('~Model$~', $file)){
        include_once(__PATH__ . 'app/model/' . $file . '.php');
        return;
    }
    //Load all the Views
    if (preg_match('~View$~', $file)){
        include_once(__PATH__ . 'app/view/' . $file . '.php');
        return;
    }
    //Load all the controllers
    if (preg_match('~Controller$~', $file)){
        include_once(__PATH__ . 'app/controller/' . $file . '.php');
        return;
    }
}

//Load the main file.
require_once('AvolansFramework.php');

//Startup the framework :)
new AvolansFramework();